$(() => {
    $("#msg").html("");

    var modpacks = {};

    function load()
    {
        if (!window.getModpacks)
        {
            $("#msg").html("No script found!");
            return false;
        }

        modpacks = getModpacks();

        if (Object.keys(modpacks).length <= 0)
        {
            $("#msg").html("No modpacks loads!");
            return false;
        }
        let html = "";
        html += "<p>Loaded " + Object.keys(modpacks).length + "</p>";
        html += "<ul>";
        for (let mod in modpacks)
        {
            html += "<li>" + modpacks[mod].name + " => " + modpacks[mod].file + "</li>"
        }
        html += "</ul>";

        $("#msg").html(html);
        $("#comp").prop("disabled", false)

        return true;
    }

    function compare()
    {
        if (Object.keys(modpacks).length <= 0)
        {
            $("#msg").html("No modpacks to compare!");
            return false;
        }

        $("main table").remove();

        var mods = {};

        $("#msg").html("Scanning mods in modpack!");
        Object.values(modpacks).forEach(modpack => {
            for (let modId in modpack.mods)
            {
                if(!mods[modId])
                    mods[modId] = modpack.mods[modId];
            }
        });


        $("#msg").html("Sorting mods!");
        var modsNames = Object.keys(mods);
        modsNames.sort();

        var tmpMods = {};
        modsNames.forEach(name => {
            tmpMods[name] = mods[name];
        });

        mods = tmpMods;
        delete tmpMods;
        delete modsNames;


        console.log(mods);

        $("#msg").html("Draw table header!");

        let table = document.createElement("table");
            // table.setAttribute("border", 1);
        let tableHeader = document.createElement("tr");
        let tableHeaderMods = document.createElement("th");
            tableHeaderMods.innerHTML = "<div>Mods</div>";

        tableHeader.appendChild(tableHeaderMods);

        for (let modpackLink in modpacks)
        {
            let modpack = modpacks[modpackLink];

            let th  = document.createElement("th");
            let div = document.createElement("div");
            let img  = document.createElement("img");
            let link = document.createElement("a");
            let file = document.createElement("div");

            img.src = modpack.icon;
            link.innerHTML = modpack.name;
            link.href = modpackLink;
            file.innerHTML = modpack.file;

            div.appendChild(img);
            div.appendChild(link);
            div.appendChild(file);
            th.appendChild(div);

            tableHeader.appendChild(th);
        }

        table.appendChild(tableHeader);

        $("#msg").html("Draw table mods!");

        for (let modName in mods)
        {
            let mod = mods[modName];

            var tr = document.createElement("tr");

            let tdMod = document.createElement("td");
            let divMod = document.createElement("div");
            let divModImg = document.createElement("img");
            divModImg.src = mod.img;
            let divModName = document.createElement("div");
            if (mod.url)
            {
                divModName = document.createElement("a");
                divModName.href = mod.url;
                divModName.target = "_blank";
            }

            divModName.innerHTML = mod.name;

            divMod.appendChild(divModImg);
            divMod.appendChild(divModName);
            tdMod.appendChild(divMod);

            tr.appendChild(tdMod);

            for (let modpackLink in modpacks)
            {
                let modpack = modpacks[modpackLink];
                let td = document.createElement("td");
                let div = document.createElement("div");

                if (modpack.mods[modName])
                {
                    div.innerHTML = "<i class='icon-plus-squared'>";
                }
                else
                {
                    div.innerHTML = "<i class='icon-minus-squared'>";
                }

                td.appendChild(div);
                tr.appendChild(td);
            }

            table.appendChild(tr);

        }

        $("#msg").html("Complete!");

        $("main").append(table);
        $("#diff").prop("disabled", false)
        $("#same").prop("disabled", false)
    }

    function btnChInn(btn, inner)
    {
        $(btn).html(inner);
    }

    var toggleDiffBool = false;
    function toggleDiff()
    {
        if(toggleSameBool) toggleSame();

        if(toggleDiffBool)
        {
            $("main table tr").each(function (index)  {
                let tr = $( this );
                tr.removeClass("row-hide");
            })

            toggleDiffBool = false;

            btnChInn("#diff", "Hide different mods")

            return;
        }

        toggleDiffBool = true;
        btnChInn("#diff", "Show different mods")

        $("main table tr").each(function (index)  {

            if (index == 0) return;

            let tr = $( this );
            let count = tr[0].children.length;

            let same;
            for (let i = 1; i < count; i++)
            {
                let child = tr[0].children[i];
                if (i == 1) same = child.innerHTML;
                else
                {
                    if (same != child.innerHTML)
                    {
                        same = false;
                        break;
                    }
                }
            }

            tr.removeClass("row-hide");
            if (!same) tr.toggleClass("row-hide");
        })
    }

    var toggleSameBool = false;
    function toggleSame()
    {
        if(toggleDiffBool) toggleDiff();

        if(toggleSameBool)
        {
            $("main table tr").each(function (index)  {
                let tr = $( this );
                tr.removeClass("row-hide");
            })

            toggleSameBool = false;
            btnChInn("#same", "Hide same mods")
            return;
        }

        toggleSameBool = true;
        btnChInn("#same", "Show same mods")

        $("main table tr").each(function (index)  {

            if (index == 0) return;

            let tr = $( this );
            let count = tr[0].children.length;

            let same;
            for (let i = 1; i < count; i++)
            {
                let child = tr[0].children[i];
                if (i == 1) same = child.innerHTML;
                else
                {
                    if (same != child.innerHTML)
                    {
                        same = false;
                        break;
                    }
                }
            }

            tr.removeClass("row-hide");
            if (same) tr.toggleClass("row-hide");
        })
    }

    function tryLoad(force = false)
    {
        if (!window.getModpacks)
        {
            $("#msg").html("No script found!");
            return;
        }
        if (force || window.cmcAutoCompare)
        {
            window.cmcAutoCompare = false;
            $("#msg").html("Try load automaticly");
            if(load()) compare();
        }
    }

    function setDefault()
    {
        $("#comp").prop("disabled", true)
        $("#diff").prop("disabled", true)
        $("#same").prop("disabled", true)
    }

    window.scriptTryLoad = tryLoad;

    setDefault();

    setTimeout(tryLoad, 500);

    setDefault();

    $("#load").on("click", () => {load();});
    $("#comp").on("click", () => {compare();})
    $("#diff").on("click", () => {toggleDiff();})
    $("#same").on("click", () => {toggleSame();})
});
